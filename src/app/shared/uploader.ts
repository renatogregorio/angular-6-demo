export class Uploader {
  queue: UploadQueue[];
  

  constructor() {
    this.queue = [];
  }

  filesEvent(event: EventTarget){
    const eventObj: MSInputMethodContext = < MSInputMethodContext > event;
    const target: HTMLInputElement = < HTMLInputElement > eventObj.target;
    const files: FileList = target.files;

    return files;
  }

  onSelectChangeDocsInternosOnlineWithService(event: EventTarget, service: any) {
    
    const files = this.filesEvent(event);
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      if (element) {
        service.uploaderDocs.queue.push(new UploadQueue(element));
      }
    }
  }
}

export class UploadQueue {
  id: string;
  file: File;
  progress: number;
  message: string;
  isCancel: boolean;
  isError: boolean;
  get isSuccess(): boolean {
    if (this.progress === 100) {
      return true;
    }
    return false;
  }

  constructor(file: File) {
    this.file = file;
    this.progress = 0;
    this.id = Guid.newGuid();
    this.message = '';
    this.isCancel = false;
    this.isError = false;
  }
}


class Guid {
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}