import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './services/usuario/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:  [ UsuarioService ]
})

export class AppComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private usuarioService : UsuarioService,
  ) { }

  ngOnInit() {
    const userParam = {"user_email": "rgregorio@vayon.com.br"};
    
    this.usuarioService.carregarUsuario(userParam)
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          if (event.status === 200) {
            sessionStorage.setItem('usuarioLogado', JSON.stringify(event.body))            
          }
        }
      },
      error => {
        this.toastr.error("Problema ao carregar dados do usuario");
      }
    );
  }

}
