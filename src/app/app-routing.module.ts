import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarLotesComponent } from "./pages/pagamentos-sinistro/listar-lotes/listar-lotes.component";
import { DetalhesLoteComponent } from "./pages/pagamentos-sinistro/detalhes-lote/detalhes-lote.component";
import { LayoutComponent } from "./layout/layout.component"

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent
  },
  {
    path: 'pagamentos-sinistro/listar-lotes',
    component: ListarLotesComponent
  },
  {
    path: 'pagamentos-sinistro/lote/:idLote',
    component: DetalhesLoteComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
