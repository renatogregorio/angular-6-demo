import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { WEBAPI_USUARIO } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient
  ) { }

  carregarUsuario(params) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_USUARIO}/selecionarUsuario` , params, {
      reportProgress: true, observe: "events"
    });
  }
}
