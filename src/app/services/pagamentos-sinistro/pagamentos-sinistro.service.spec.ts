import { TestBed } from '@angular/core/testing';

import { PagamentosSinistroService } from './pagamentos-sinistro.service';

describe('PagamentosSinistroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PagamentosSinistroService = TestBed.get(PagamentosSinistroService);
    expect(service).toBeTruthy();
  });
});
