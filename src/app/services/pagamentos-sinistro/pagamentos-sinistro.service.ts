import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Uploader } from '../../shared/uploader';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { WEBAPI_PAG_SINISTROS } from '../../../environments/environment';

@Injectable()
export class PagamentosSinistroService {

  uploaderDocs: Uploader = new Uploader();

  constructor(
    private http: HttpClient
  ) { }

  uploadLote(fileBase64 : any) : Observable<HttpEvent<any>> {

    const selectedFile = this.uploaderDocs.queue[0];

    if (selectedFile) {
      const usuarioLogado = JSON.parse(sessionStorage.getItem('usuarioLogado'));
      const json = {
        ds_lote_nome_arquivo : selectedFile.file.name,
        ds_lote_conteudo_arquivo_base64 : fileBase64, //convert to base 64
        // id_lote_usuario_insercao : usuarioLogado.id_usuario,
        id_lote_usuario_insercao : usuarioLogado.user_id,
        ds_nome_usuario: usuarioLogado.real_name,
        ds_email_usuario: usuarioLogado.user_email
        // id_lote_usuario_insercao : 6494
      }
      
      return this.http.post(`${WEBAPI_PAG_SINISTROS}/uploadArquivoLote` , json, {
        reportProgress: true, observe: "events"
      });
    }
  }

  carregarLotes(filters : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/selecionarLotes` , filters, {
      reportProgress: true, observe: "events"
    });
  }

  carregarLotesPeriodo(filters : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/selecionarLotesPorPeriodo` , filters, {
      reportProgress: true, observe: "events"
    });
  }

  carregarLotesUsuario(filters : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/selecionarLotesPorUsuario` , filters, {
      reportProgress: true, observe: "events"
    });
  }

  carregarLotesStatus(filters : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/selecionarLotesPorStatus` , filters, {
      reportProgress: true, observe: "events"
    });
  }

  downloadLote(lote : any) : Observable<HttpEvent<any>> {
    
    return this.http.post(`${WEBAPI_PAG_SINISTROS}/downloadArquivoLote` , lote, {
      reportProgress: true, observe: "events"
    });
  }

  carregarLote(params : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/selecionarSinistrosLote` , params, {
      reportProgress: true, observe: "events"
    });
  }

  atualizarCoberturaSinistro(params : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/atualizarCoberturasSinistros` , params, {
      reportProgress: true, observe: "events"
    });
  }

  enviarPagamentoEbao(params : any) : Observable<HttpEvent<any>> {

    return this.http.post(`${WEBAPI_PAG_SINISTROS}/enviarLoteParaPagamentoEbao` , params, {
      reportProgress: true, observe: "events"
    });
  }

  downloadLog(lote : any) : Observable<HttpEvent<any>> {
    
    return this.http.post(`${WEBAPI_PAG_SINISTROS}/downloadArquivoLogLote` , lote, {
      reportProgress: true, observe: "events"
    });
  }
}
