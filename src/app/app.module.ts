import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatIconModule } from '@angular/material';
import { ListarLotesComponent } from './pages/pagamentos-sinistro/listar-lotes/listar-lotes.component';
import { DetalhesLoteComponent } from './pages/pagamentos-sinistro/detalhes-lote/detalhes-lote.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { PaginationComponent } from './pagination/pagination.component';
import { LoadingComponent } from './loading/loading.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';

 
@NgModule({
  declarations: [
    AppComponent,
    ListarLotesComponent,
    DetalhesLoteComponent,
    LayoutComponent,
    PaginationComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    MatIconModule,
    FormsModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgbModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
