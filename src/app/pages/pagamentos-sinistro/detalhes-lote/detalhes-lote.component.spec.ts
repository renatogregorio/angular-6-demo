import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalhesLoteComponent } from './detalhes-lote.component';

describe('DetalhesLoteComponent', () => {
  let component: DetalhesLoteComponent;
  let fixture: ComponentFixture<DetalhesLoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalhesLoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalhesLoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
