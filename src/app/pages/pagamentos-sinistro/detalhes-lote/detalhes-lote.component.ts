import * as $ from 'jquery';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { PagamentosSinistroService } from '../../../services/pagamentos-sinistro/pagamentos-sinistro.service';
import { ToastrService } from 'ngx-toastr';
import { Pagination } from '../../../pagination/pagination'
import { PaginationComponent } from '../../../pagination/pagination.component';

@Component({
  selector: 'app-detalhes-lote',
  templateUrl: './detalhes-lote.component.html',
  styleUrls: ['./detalhes-lote.component.css'],
  providers:  [ PagamentosSinistroService, Pagination, PaginationComponent ]
})
export class DetalhesLoteComponent implements OnInit {

  private idLote          : string;
  private sinistros       : Array<Object>;
  private lote            : any;
  private totalRegistros  : number;
  private showPagination  : Boolean;
  private pesquisandoGif  : Boolean;
  
  private todasCoberturasSelecionadas : Boolean;

  constructor(
    private toastr: ToastrService,
    private pagamentosSinistroService : PagamentosSinistroService,
    private pagination: Pagination,
    private paginationComp: PaginationComponent,
    private activeteRoute: ActivatedRoute,
    private router: Router
  ) {
    this.pagination.getState()
    .subscribe(obj => {
      this.carregarLote(obj.page, null);
    });
   }

  ngOnInit() {
     $("html, body").animate({ scrollTop: 0 }, "slow");
    this.idLote = this.activeteRoute.snapshot.params.idLote;
    this.carregarLote(1, null);
  }

  carregarLote(page : any, order : any) {

    this.sinistros = [];
    const params = {
      id_lote: this.idLote,
      int_numero_pagina: page,
      int_quantidade_registros: this.paginationComp.totalPerPage,
      des_coluna_ordenacao: null, 
      des_tipo_ordenacao: null
    }

    this.pesquisandoGif = true;
    this.pagamentosSinistroService.carregarLote(params)
    .subscribe(
      event => {        
        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            this.sinistros = event.body.sinistros;
            this.lote = event.body.lote;
            this.totalRegistros = event.body.lote.int_lote_total_registros;
            this.todasCoberturasSelecionadas = event.body.ic_todas_coberturas_lote_selecionadas;

            if (page == 1 && this.totalRegistros > 0) {
              this.showPagination = true;
            }
          }
        }
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Problema ao efetuar busca de listagem de lotes");
      }
    );
  }

  changeCobertura(sinistro) {
    const usuarioLogado = JSON.parse(sessionStorage.getItem('usuarioLogado'));

    const jsonSend = {
      id_usuario: usuarioLogado.user_id,
      ds_nome_usuario: usuarioLogado.real_name,
      ds_email_usuario: usuarioLogado.user_email,
      id_lote: this.idLote,
      coberturasSinistros: [
        {
          id_sinistro_lote: sinistro.id_sinistro_lote,
          id_cobertura_sinistro: sinistro.cobertura.id_cobertura_sinistro
        }
      ]
    }

    this.pesquisandoGif = true;
    this.pagamentosSinistroService.atualizarCoberturaSinistro(jsonSend)
    .subscribe(
      event => {        
        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            this.todasCoberturasSelecionadas = event.body.ic_todas_coberturas_lote_selecionadas;            
          }
        }
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Problema ao efetuar busca de listagem de lotes");
      }
    );
  }

  mostrarEnviarPagamentoEbao() : Boolean{
    return this.lote.ds_lote_sub_status == "Aguardando Classificação"
  }

   mostrarReenviarPagamentoEbao() : Boolean{
    return this.lote.ds_lote_sub_status == "Erro procesamento eBao" || this.lote.ds_lote_sub_status == "Processado Parcialmente"
  }

  enviarPagamentoEbao() {
    if (!this.todasCoberturasSelecionadas) {
      this.toastr.warning("Selecione todos as coberturas para enviar o lote para pagamento");
      return false;
    }

    const usuarioLogado = JSON.parse(sessionStorage.getItem('usuarioLogado'));

    const jsonSend = {
      id_usuario: usuarioLogado.user_id,
      ds_nome_usuario: usuarioLogado.real_name,
      ds_email_usuario: usuarioLogado.user_email,
      id_lote: this.idLote,
    };

    this.pagamentosSinistroService.enviarPagamentoEbao(jsonSend)
    .subscribe(
      event => {        
        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            this.carregarLote(1, null);
            this.toastr.success("Pagamento enviado com sucesso, aguarde o processamento");
          }
        }
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Problema ao enviar pagamento para ebao");
      }
    );
  }

   downloadLog() {
    
    this.pesquisandoGif = true;
    this.pagamentosSinistroService.downloadLog({ id_lote: this.idLote})
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            const file = event.body;
            this.downloadCSV(file.ds_lote_conteudo_arquivo_base64, file.ds_lote_nome_arquivo);
          }
        }
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Erro ao efetuar download do lote");
      }
    )
  }

  downloadCSV(file : string, fileName : string) {
    const linkSource = `data:application/csv;base64,${file}`;
    const downloadLink = document.createElement("a");

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}
