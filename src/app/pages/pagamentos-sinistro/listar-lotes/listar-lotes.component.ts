import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { PagamentosSinistroService } from '../../../services/pagamentos-sinistro/pagamentos-sinistro.service';
import { ToastrService } from 'ngx-toastr';
import { Pagination } from '../../../pagination/pagination'
import { PaginationComponent } from '../../../pagination/pagination.component';
import { BsDatepickerConfig, BsLocaleService, formatDate } from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-lotes',
  templateUrl: './listar-lotes.component.html',
  styleUrls: ['./listar-lotes.component.css'],
  providers:  [ PagamentosSinistroService, Pagination, PaginationComponent ],
})
export class ListarLotesComponent implements OnInit {

  private filtros = [
    {
      cod: '0',
      nome: 'Selecione'
    },
    {
      cod: 'PER',
      nome: 'Período'
    },
    {
      cod: 'USR',
      nome: 'Usuário'
    }
  ]; 
  
  private dt_inicio         : string;
  private dt_fim            : string;
  private bsConfig          : Partial<BsDatepickerConfig>;
  private nomeUsuario       : string;
  private statusSelecionado : string;

  private filtroPrincipal   : String;
  private filtroSelecionado : Object;
  private arquivoTela       : Object;
  private arquivoInput      : String;
  private order             : any;

  private lotes             : Array<Object>;
  private statusLote        : Array<Object>;  
  private totalRegistros    : number;
  private totalRegistrosLbl : number;

  private zerarFiltros    : Boolean;
  private showPagination  : Boolean;
  private pesquisandoGif  : Boolean;

  private sortReverse     : Boolean;
  private sortType        : string;

  constructor(  
    private toastr: ToastrService,
    private pagamentosSinistroService : PagamentosSinistroService,
    private pagination: Pagination,
    private paginationComp: PaginationComponent,
    private router: Router
  ) {
    this.pagination.getState()
    .subscribe(obj => {
      this.carregarLotes(obj.page);
    });
  }

  ngOnInit() {            
    this.inicializarTela();
  }

  changeFilter() {
    if(this.filtroPrincipal == "PER"){
      this.dt_inicio = moment().subtract(1, 'month').format('DD/MM/YYYY');
      this.dt_fim = moment().format('DD/MM/YYYY');
    }
  }

  inicializarTela() {
    this.sortReverse = true;
    this.filtroPrincipal = "0";
    this.totalRegistros = 0;
    this.showPagination = false;
    this.statusSelecionado = null;
    this.bsConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      dateImputFormat: 'DD/MM/YYYY',
      showWeekNumbers: false
    })
    this.zerarOrder();
    this.carregarLotes(1);
  }

  filterStatus(status: any) {
    if (status.int_quantidade == 0) {
      alert('Não existem lotes nessa situação.')
    } else {
      this.statusSelecionado = status.status;
      this.showPagination = false;
      this.zerarOrder();
      this.carregarLotesStatus(1);
    }
  }

  selecionaDoc(event: EventTarget) {
    const arquivos = this.pagamentosSinistroService.uploaderDocs.filesEvent(event);
    
    if (arquivos && arquivos.length > 0) {
      //sempre considerando o primeiro arquivo do array pois sempre terá um só
      const arquivoValido = this.validateFileExtension(arquivos[0]);
      if (!arquivoValido) {
        this.toastr.error("É apenas permitido arquivos com a extensão .csv");
      } else {
        this.pagamentosSinistroService.uploaderDocs.onSelectChangeDocsInternosOnlineWithService(event, this.pagamentosSinistroService);
        this.arquivoTela = {
          filename: arquivos[0].name,
          size: arquivos[0].size
        };
      }
    }
  }

  validateFileExtension (file : any) {
    const extension = file.name.substring(file.name.lastIndexOf('.')+1, file.name.length);
    return ['csv'].includes(extension);
  }

  excluirArquivo() {
    this.arquivoTela = null;
    this.arquivoInput = "";
  }

  readableBytes(bytes: number) {
    var i = Math.floor(Math.log(bytes) / Math.log(1024)),
    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    return <any>(bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
  }

  async uploadArquivo() {
    this.pesquisandoGif = true;
    const base64file = await this.getBase64(this.pagamentosSinistroService.uploaderDocs.queue[0].file);
    this.pagamentosSinistroService.uploadLote(base64file)
    .subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          console.log(Math.round(100 * event.loaded / event.total));
        }

        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            this.toastr.success("Upload realizado com sucesso");
            this.excluirArquivo();
            this.zerarFiltros = true;
            this.statusSelecionado = null;
            this.zerarOrder();
            this.carregarLotes(1);
          } else {
            this.toastr.error("Problema ao efetuar upload");
          }
        }
        
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Problema ao efetuar busca de listagem de lotes");
      }
    );
  }

  async getBase64(file : any) {
    return await new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.replace('data:text/csv;base64,', ''));
      reader.onerror = error => reject(error);
    });
  }

  pesquisar(page : any) {
    this.showPagination = false;
    this.sortType = null;
    this.pagination.setActiveState(0);
    this.statusSelecionado = null;
    this.zerarOrder();
    this.carregarLotes(page);
  }

  carregarLotes(page) {
    
    if (this.validacaoFiltro()) {
      this.lotes = [];

      switch (this.filtroPrincipal) {
        case 'PER': 
          this.carregarLotesPeriodo(page);
          break;

        case 'USR':
          this.carregarLotesUsuario(page);
          break;

        default:
          if (this.statusSelecionado) {
            this.carregarLotesStatus(page);
          } else {
            this.carregarLotesSemFiltro(page);
          } 
      }
    }    
  }

  carregarLotesSemFiltro(page : any) {
    
    const jsonFiltros = {
      int_numero_pagina: page,
      int_quantidade_registros: this.paginationComp.totalPerPage,
      des_coluna_ordenacao: null,
      des_tipo_ordenacao: null
    };

    if (this.order) {
      jsonFiltros.des_coluna_ordenacao = this.order.des_coluna_ordenacao;
      jsonFiltros.des_tipo_ordenacao   = this.order.des_tipo_ordenacao;
    }
    
    this.pesquisandoGif = true;
    this.pagamentosSinistroService.carregarLotes(jsonFiltros)
    .subscribe(
      event => {
        this.processarRetornoLotes(page, event);
      },
      error => {
        this.mensagemErroBusca();
      }
    );
  }

  carregarLotesPeriodo(page : any) {
    
    const jsonFiltros = {
      int_numero_pagina: page,
      int_quantidade_registros: this.paginationComp.totalPerPage,
      dt_data_inicial: moment(this.dt_inicio).format('YYYY-MM-DD'),
      dt_data_final: moment(this.dt_fim).format('YYYY-MM-DD'),
      des_coluna_ordenacao: null,
      des_tipo_ordenacao: null
    };

    if (this.order) {
      jsonFiltros.des_coluna_ordenacao = this.order.des_coluna_ordenacao;
      jsonFiltros.des_tipo_ordenacao   = this.order.des_tipo_ordenacao;
    }

    this.pesquisandoGif = true;
    this.pagamentosSinistroService.carregarLotesPeriodo(jsonFiltros)
    .subscribe(
      event => {
        this.processarRetornoLotes(page, event);
      },
      error => {
        this.mensagemErroBusca();
      }
    );
  }

  carregarLotesUsuario(page : any) {
    
    const jsonFiltros = {
      int_numero_pagina: page,
      ds_nome_usuario: this.nomeUsuario,
      int_quantidade_registros: this.paginationComp.totalPerPage,
      des_coluna_ordenacao: null,
      des_tipo_ordenacao: null
    };

    if (this.order) {
      jsonFiltros.des_coluna_ordenacao = this.order.des_coluna_ordenacao;
      jsonFiltros.des_tipo_ordenacao   = this.order.des_tipo_ordenacao;
    }

    this.pesquisandoGif = true;
    this.pagamentosSinistroService.carregarLotesUsuario(jsonFiltros)
    .subscribe(
      event => {
        this.processarRetornoLotes(page, event);
      },
      error => {
        this.mensagemErroBusca();
      }
    );
  }

  carregarLotesStatus(page : any) {
    
    const jsonFiltros = {
      int_numero_pagina: page,
      int_quantidade_registros: this.paginationComp.totalPerPage,
      ds_lote_status_tela: this.statusSelecionado,
      des_coluna_ordenacao: null,
      des_tipo_ordenacao: null
    };

    if (this.order) {
      jsonFiltros.des_coluna_ordenacao = this.order.des_coluna_ordenacao;
      jsonFiltros.des_tipo_ordenacao   = this.order.des_tipo_ordenacao;
    }

    this.pesquisandoGif = true;
    this.pagamentosSinistroService.carregarLotesStatus(jsonFiltros)
    .subscribe(
      event => {
        this.processarRetornoLotes(page, event);
      },
      error => {
        this.mensagemErroBusca();
      }
    );
  }

  processarRetornoLotes(page : any, event : any) {
    if (event.type === HttpEventType.Response) {
      this.pesquisandoGif = false;
      if (event.status === 200) {
        this.lotes = event.body.lotes;
        this.totalRegistros = event.body.int_total_lotes;
        
        if (!this.statusSelecionado) {
          this.statusLote = event.body.dadosStatusLotes;
          this.totalRegistrosLbl = event.body.int_total_lotes;
        }

        if (page == 1 && this.totalRegistros > 0) {
          this.showPagination = true;
        }
      }
    }
  }

  mensagemErroBusca() {
    this.pesquisandoGif = false;
    this.toastr.error("Problema ao efetuar busca de listagem de lotes");
  }

  validacaoFiltro() : Boolean {
    
    if (this.zerarFiltros) {
      this.filtroPrincipal = '0';
    }

    switch (this.filtroPrincipal) {
      case 'PER':
        if(!this.dt_inicio || !this.dt_fim) {
          this.toastr.warning("Período de datas inválidas");
          return false;
        }
        break;

      case 'USR':
        if(!this.nomeUsuario) {
          this.toastr.warning("Usuário inválido");
          return false;
        }
        break;
    }

    return true;
  }

  downloadLote(lote : any) {
    
    this.pesquisandoGif = true;
    this.pagamentosSinistroService.downloadLote({ id_lote: lote.id_lote})
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          this.pesquisandoGif = false;
          if (event.status === 200) {
            const file = event.body;
            this.downloadCSV(file.ds_lote_conteudo_arquivo_base64, file.ds_lote_nome_arquivo);
          }
        }
      },
      error => {
        this.pesquisandoGif = false;
        this.toastr.error("Erro ao efetuar download do lote");
      }
    )
  }

  downloadCSV(file : string, fileName : string) {
    const linkSource = `data:application/csv;base64,${file}`;
    const downloadLink = document.createElement("a");

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  detalhesLote(idLote : number) {
    this.router.navigate([`/pagamentos-sinistro/lote/${idLote}/`]);
  }

  ordenarResultado(campo : string) {
    this.sortReverse = !this.sortReverse;
    this.sortType = campo;
    this.order = {
      des_coluna_ordenacao: campo,
      des_tipo_ordenacao: null
    }

    if (this.sortReverse) {
      this.order.des_tipo_ordenacao = 'asc';
    } else {
      this.order.des_tipo_ordenacao = 'desc'
    }

    if (this.statusSelecionado) {
      this.carregarLotesStatus(1);
    } else {
      this.carregarLotes(1);
    }    
  }

  zerarOrder() {
    this.order = {
      des_coluna_ordenacao: null,
      des_tipo_ordenacao: null
    }
  }

  showEditButon(status: string){
    return ['Descartado', 'Erro', 'Processado'].indexOf(status) === -1
  }

  showVisionButon(status: string){
    return ['Descartado', 'Erro', 'Processado'].indexOf(status) > -1
  }
}
