import { Component, OnInit, Input } from '@angular/core';
import { Pagination } from './pagination';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})

export class PaginationComponent implements OnInit {

  @Input()
    public totalRegistros;
  @Input()
    public maxSize;

  public totalPerPage: number = 20;
  private allPages: Array<number>
  private pagesList :any = [0];
  private active = 0;

  constructor(private pagination: Pagination){
  }

  ngOnInit() {
    let maxPages = parseInt((this.totalRegistros / this.totalPerPage).toFixed(1));
    const restDivision = (this.totalRegistros % this.totalPerPage);

    if (this.totalRegistros > 0) {
      if(maxPages == 0 && restDivision == 0) maxPages += 1;
      if(restDivision > 0) maxPages += 1;
      this.allPages = Array.from({ length: maxPages }, (e, i) => i);
    }

    this.pagination.getActiveState()
    .subscribe(obj => {
      this.active = obj;
    });
  }

  selectPage(page){
    this.active = page;
    let obj = {
      page: page + 1,
      totalPerPage: this.totalPerPage
    }

    this.pagination.setState(obj);
  }
}
