import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class Pagination {

  private pagination = new Subject<any>();
  private active = new Subject<any>();

  setState(state: any) {
    this.pagination.next(state);
  }

  getState(): Observable<any> {
    return this.pagination.asObservable();
  }

  setActiveState(state: any) {
    this.active.next(state);
  }

  getActiveState(): Observable<any> {
    return this.active.asObservable();
  }
}